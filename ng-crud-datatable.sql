-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2017 at 06:57 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ng-crud-datatable`
--

-- --------------------------------------------------------

--
-- Table structure for table `angularcode_customers`
--

CREATE TABLE `angularcode_customers` (
  `customerNumber` int(11) NOT NULL,
  `customerName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `postalCode` varchar(15) DEFAULT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `angularcode_customers`
--

INSERT INTO `angularcode_customers` (`customerNumber`, `customerName`, `email`, `address`, `city`, `state`, `postalCode`, `country`) VALUES
(103, 'Atelier graphiques', 'Nantes@gmail.coms', '54, rue Royales', 'Nantess', NULL, '44000', 'Frances'),
(112, 'Signal Gift Stores', 'LasVegas@gmail.com', '8489 Strong St.', 'Las Vegas', 'NV', '83030', 'USA'),
(114, 'Australian Collectors, Co.', 'Melbourne@gmail.com', '636 St Kilda Road', 'Melbourne', 'Victoria', '3004', 'Australia'),
(119, 'La Rochelle Gifts', 'Nantes@gmail.com', '67, rue des Cinquante Otages', 'Nantes', NULL, '44000', 'France'),
(121, 'Baane Mini Imports', 'Stavern@gmail.com', 'Erling Skakkes gate 78', 'Stavern', NULL, '4110', 'Norway'),
(124, 'Mini Gifts Distributors Ltd.', 'SanRafael@gmail.com', '5677 Strong St.', 'San Rafael', 'CA', '97562', 'USA'),
(125, 'Havel & Zbyszek Co', 'Warszawa@gmail.com', 'ul. Filtrowa 68', 'Warszawa', NULL, '01-012', 'Poland'),
(128, 'Blauer See Auto, Co.', 'Frankfurt@gmail.com', 'Lyonerstr. 34', 'Frankfurt', NULL, '60528', 'Germany'),
(129, 'Mini Wheels Co.', 'SanFrancisco@gmail.com', '5557 North Pendale Street', 'San Francisco', 'CA', '94217', 'USA'),
(131, 'Land of Toys Inc.', 'NYC@gmail.com', '897 Long Airport Avenue', 'NYC', 'NY', '10022', 'USA'),
(141, 'Euro+ Shopping Channel', 'Madrid@gmail.com', 'C/ Moralzarzal, 86', 'Madrid', NULL, '28034', 'Spain'),
(145, 'Danish Wholesale Imports', 'Kobenhavn@gmail.com', 'Vinbltet 34', 'Kobenhavn', NULL, '1734', 'Denmark'),
(146, 'Saveley & Henriot, Co.', 'Lyon@gmail.com', '2, rue du Commerce', 'Lyon', NULL, '69004', 'France'),
(148, 'Dragon Souveniers, Ltd.', 'Singapore@gmail.com', 'Bronz Sok.', 'Singapore', NULL, '079903', 'Singapore'),
(151, 'Muscle Machine Inc', 'NYC@gmail.com', '4092 Furth Circle', 'NYC', 'NY', '10022', 'USA'),
(157, 'Diecast Classics Inc.', 'Allentown@gmail.com', '7586 Pompton St.', 'Allentown', 'PA', '70267', 'USA'),
(161, 'Technics Stores Inc.', 'Burlingame@gmail.com', '9408 Furth Circle', 'Burlingame', 'CA', '94217', 'USA'),
(166, 'Handji Gifts& Co', 'Singapore@gmail.com', '106 Linden Road Sandown', 'Singapore', NULL, '069045', 'Singapore'),
(167, 'Herkku Gifts', 'Bergen@gmail.com', 'Brehmen St. 121', 'Bergen', NULL, 'N 5804', 'Norway  '),
(169, 'User One', 'userone@gmail.com', 'Yangon', 'Yangon', NULL, NULL, 'Myanmar'),
(170, 'User Two', 'usertwo@gmail.com', 'Yangon', 'Yangon', NULL, NULL, 'Myanmar'),
(171, 'User Four', 'four@gmail.com', 'Yangon', 'Yangon', NULL, NULL, 'Myanmar');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_email_address` varchar(100) NOT NULL,
  `emp_full_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`emp_id`, `emp_name`, `emp_email_address`, `emp_full_name`) VALUES
(24, 'sabuzcode', 'everythingkst@gmail.com', 'Hello Sabuz'),
(25, 'pineapple', 'pineapple@gmail.com', 'Hello Sabuz'),
(26, 'sabuz', 'everythingkst@gmail.com', 'Hello Sabuz'),
(28, 'sabuz', 'everythingkst@gmail.com', 'Hello Sabuz'),
(29, 'sabuz', 'everythingkst@gmail.com', 'Hello Sabuz'),
(30, 'sabuz', 'everythingkst@gmail.com', 'Hello Sabuz'),
(34, 'Uddin', 'uddin@gmail.com', 'Shihab Uddin'),
(35, 'simon', 'simon@gmail.com', 'Saymon'),
(37, 'Arzu', 'arzu@yahoo.com', 'Hello world'),
(38, 'sabuz', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(39, 'sabuzcode', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(40, 'sabuzcode', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(41, 'sabuzcode', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(42, 'frsmgt@gmail.com', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(43, 'frsmgt@gmail.com', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(44, 'frsmgt@gmail.com', 'everythingkst@gmail.com', 'Hello Firoz to home'),
(45, 'sabuz', 'everythingkst@gmail.com', 'Firoz Rahman Sabuz'),
(46, 'sabuz', 'everythingkst@gmail.com', 'Firoz Rahman Sabuz'),
(48, 'sabuz', 'everythingkst@gmail.com', 'Firoz Rahman Sabuz'),
(50, 'candy', 'candy@gmail.com', 'Firoz Rahman Sabuz'),
(51, 'banana@gmail.com', 'banana@gmail.com', 'Hello Firoz to home'),
(52, 'apple', 'apple@gmail.com', 'Hello Firoz to home'),
(53, 'Smith', 'smith@gmail.com', 'Smith John'),
(54, 'Willian', 'william@gmail.com', 'William John'),
(55, 'One', 'userone@gmail.com', 'User One'),
(56, 'Two', 'two@gmail.com', 'User Two');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angularcode_customers`
--
ALTER TABLE `angularcode_customers`
  ADD PRIMARY KEY (`customerNumber`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angularcode_customers`
--
ALTER TABLE `angularcode_customers`
  MODIFY `customerNumber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
